////////////
// modURL //
////////////
var modURL = {
  moduleName: 'docs',

  setURLs: function(){

    this.read = utilJS.buildLink({    targetModule : this.moduleName,
                                         action    : '/getDocs',
                                         args      : ''
                                    });

    this.remove = utilJS.buildLink({    targetModule : this.moduleName,
                                           action    : '/removeDocs',
                                           args      : ''
                                      });

    this.rename = utilJS.buildLink({    targetModule : this.moduleName,
                                           action    : '/renameDoc',
                                           args      : ''
                                      });

    this.upload = utilJS.buildLink({    targetModule : this.moduleName,
                                           action    : '/uploadDoc',
                                           args      : ''
                                      });
    this.idx = utilJS.buildLink({    targetModule : this.moduleName,
                                           action    : '/index',
                                           args      : ''
                                      });
    this.download = utilJS.buildLink({    targetModule : this.moduleName,
                                           action    : '/downloadDoc',
                                           args      : ''
                                      });
    this.downloadModule = utilJS.buildLink({    targetModule : this.moduleName,
                                                action    : '/downloadModule',
                                                args      : ''
                                      });
    this.move = utilJS.buildLink({    targetModule   : this.moduleName,
                                           action    : '/moveDoc',
                                           args      : ''
                                      });
    this.generateLink = utilJS.buildLink({ targetModule   : this.moduleName,
                                           action         : '/generateLink',
                                           args           : ''
                                      });
    this.containsDoc = utilJS.buildLink({ targetModule   : this.moduleName,
                                           action         : '/containsDoc',
                                           args           : ''
                                      });
    this.canEditDoc = utilJS.buildLink({ targetModule   : this.moduleName,
                                           action         : '/canEditDoc',
                                           args           : ''
                                      });
    this.savePermission = utilJS.buildLink({ targetModule   : this.moduleName,
                                           action         : '/savePermission',
                                           args           : ''
                                      });
    this.getContacts = utilJS.buildLink({  targetModule : 'contacts',
                                              action    : '/getContacts',
                                              args      : '/format/json'
                                        });


  }

};
