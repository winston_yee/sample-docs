////////////
// docsJS //
////////////

var docsJS = (function($){

  return{
    DOM: $('body'),
    st_id: utilJS.getUrlValRight('st_id') ? utilJS.getUrlValRight('st_id') : '',
    pgm_id: utilJS.getUrlValRight('pgm_id') ? utilJS.getUrlValRight('pgm_id') : '',
    pg_id: '',
    pgmDocID: utilJS.getUrlValRight('pgmDocID') ? utilJS.getUrlValRight('pgmDocID') : '',
    getCmdtsInfo: utilJS.getCmdtsInfo(),

    init: function(){

      this.addEventListeners();

      // Set up modURL object that holds all the routes
      modURL.setURLs();

      // Initialize kendoui datasoure
      modDocDS.init();

    },

    addEventListeners: function () {
      var elDocs = $('.docs');
      //When user clicks on columns headers to reorder documents
      elDocs.('.header').on('click', function(e){ modSortDocs.sortHandler(e); });

      //When user clicks on any of the toolbars (i.e. Download/Export, remove, rename, move, newfolder, upload)
      elDocs.('.row-toolbar-action, .row-toolbarRight-action').on('click', function(e){ modToolbar.clickHandler(e); });

      //When user clicks on document link
      $('body').on('click', '.docs .docName', function(e){ modLink.linkHandler(e); });

      // When user selects a document
      $('body').on('click', '.docs .rowFiles', function(e){ modSelect.selectHandler(e); });

    }

  };

})();


////////////////
// modUpload //
////////////////

var modUpload = (function(){

  var elDocs =  $('.docs');
  var elFile = elDocs.find('#file');

  return{

    init: function(){
      // If file element exists then remove it
      if( elFile.length ){
        this.hideUpload();
        return;
      }

      this.showUpload();
    },

    showUpload: function(){
      var el = $('<input name="file" id="file" type="file" />');

      this.elDocs.find('.rowUpload').append( el );
      this.elDocs.find('#file').kendoUpload({
          async: {
              saveUrl: modURL.upload + '?st_id=' + docsJS.st_id,
              removeUrl: "remove"
          },
          complete: function(e) {
            modDocDS.refresh();
            modUpload.hideUpload();
          }
      });

      $('.k-upload-button span').text( $('#btnUploadText').val() );
    },

    hideUpload: function(){
      var elUpload = elDocs.find('.k-upload');
      elUpload.remove();
    }
  };

})();

////////////
//modLink //
////////////
modLink = (function(){
  var el       = $(e.currentTarget);
  var rowFiles = el.closest('.rowFiles');
  var uid      = rowFiles.attr('data-uid');
  var data     = modDocDS.getDS.getByUid(uid);
  var baseUrl  = '/cmdts/modules/docs/views/index.html';

  return{
    linkHandler: function(e){
      e.stopPropagation();

      if( data.type === 'folder' && data.id > -1){

        url = baseUrl + '?st_id=' + data.id + '&pg_id=' + docsJS.pg_id + '&l_id=' + docsJS.l_id;

        // Show documents in that folder
        document.location.href = url;
      }

      else if (data.type === 'file'){
        var url = docsJS.getCmdtsInfo.showFile + '?d_id=' + data.id +'&ext=' + data.ext + '&name=' + encodeURIComponent( data.name ) ;

        // Serve file to user
        window.open(url , '_self','','');
      }

    }
  };

});


/////////////////
// modSortDocs //
/////////////////
var modSortDocs = (function(){

  var target = $(e.target);
  var name   = target.attr('data-header-name');
  var dir    = target.attr('data-header-dir');
  var icon   = target.children();

  var sortDataSource = function( field, dir ){
      modDocDS.getDS.sort({ field: field, dir: dir });
  };

  var sortDir = function( target, dir, icon, iconDir){
      $('.header').removeAttr('data-header-dir');
      $('.rowHeader').find('i').removeClass();
      target.attr('data-header-dir', dir);
      icon.addClass('fa fa-caret-' + iconDir);
  };

  return{
    sortHandler: function(e){
        switch(dir){
          case 'asc':
           this.sortDataSource(name, 'desc');
           this.sortDir(target, 'desc', icon, 'down');
          break;
          case 'desc':
            this.sortDataSource(name, 'asc');
            this.sortDir(target, 'asc', icon, 'up');
          break;
          default:
            this.sortDataSource(name, 'asc');
            this.sortDir(target, 'asc', icon, 'up');
          break;
        }

    }

};

})($);



(function(){
  docsJS.init();
})();