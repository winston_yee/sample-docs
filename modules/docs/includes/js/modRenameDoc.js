//////////////////
// modRenameDoc //
//////////////////
var modRenameDoc = {

  init: function(){

    // Get selected/checked doc
    this.arrChecked  = modSelect.getArrChecked();

    // One document must be selected
    if( !this.arrChecked.length ){
      toastr.warning('Please select one document to rename');
      return;
    }

    // Get doc values
    this.docId        = this.arrChecked[0].id;
    this.docType      = this.arrChecked[0].type;
    this.elSelectdDoc = $('#' + this.docId ).find('.spanDoc .docName');
    this.origDocName  = $.trim( this.elSelectdDoc.text() );

    // Insert input in new doc row
    this.insertInput();

    // Add event listerns on new input
    this.addEventListeners();

  },

  insertInput: function(){
    this.elRename    = $('<input>').attr('type','text').attr('id','elRename');

    // Place input in row
    this.elSelectdDoc .text('')
                      .append( this.elRename );

    // Add in original name
    // Put cursor in new input
    this.elRename .val( this.origDocName )
                  .select();
  },

  addEventListeners: function(){
    var _this = this;

    this.elRename.on('keyup', function(e){
      //Press Enter
      if(e.keyCode === 13) {
          _this.renameFileAjax();
        }
      //Press ESC
      else if(e.keyCode === 27){
        _this.restoreDocName();
      }

    });

    // Revert back to original name if input loses focus
    this.elRename.on('focusout', function(e){
      _this.restoreDocName();
    });

    // When renaming a folder, stop from viewing files in folder if user clicks in input.
    this.elRename.on('click', function(e){ e.stopPropagation();});
  },

  renameFileAjax: function(){
    var _this = this;
    var data  =  {  id      : this.docId,
                   name     : this.elRename.val(),
                   oldName  : this.origDocName,
                   type     : this.docType,
                   st_id    : docsJS.st_id
                };

    $.post( modURL.rename, data )
      .done( function(result){
        var msg = _this.origDocName + ' was renamed to ' +  _this.elRename.val();

        // Refresh kendoui datasoure and render template
        modDocDS.refresh();

        //Notify user success
        toastr.success( msg, '', {timeOut: 7000} );

      })
      .fail( function( event, jqxhr, errMsg, exception ){
        // Notify user error
        toastr.error( folderName + 'could not be renamed' , '', {timeOut: 7000} );
        console.log(errMsg);
      });

  },

  restoreDocName: function(){
    this.elRename.remove();
    this.elSelectdDoc.text( this.origDocName );
  }
};