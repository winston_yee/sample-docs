////////////
// moveDocJS //
////////////

var moveDocJS = (function($){

  var initWindow = function(){
    // Init kendoui Window/Modal
    mod WindowJS.init( 'Move file to.. ');

    // Get template html for folder tree
    var template = $('#moveToFolderTemplate');
    var html     = template.html();

    //Add to template kendoui window
    $('#docsWindow').html( html );
  };

  return {

    init: function(){

      if( !modSelect.getArrChecked().length ){
        toastr.warning('No files were selected');
        return;
      }

      // Init kendoui window
      this.initWindow();

      // Init kendoui treeview
      modTreeDS.init();

      this.addEventListeners();
    },

    addEventListeners: function(){
      $('#btnMove').on('click', function(e){ modMoveToFolder.init(); });
      $('#btnCancel').on('click', function(e){ mod  WindowJS.close(); });
    }

  };

})($);


var modTreeDS = (function($, kendo){

  var initTreeDS = function(){

    this.getDS = new kendo.data.HierarchicalDataSource({
       transport: {
           read: {
               url: modURL.read,
               dataType: "json"
           }
       },
      schema: {
        model: {
                  id: "id",
                  hasChildren: "hassubfolders",

                  fields: {
                            hassubfolders: {type: "boolean"}
                          }
           }
       },
       filter: { field: "type",  value: "folder" },

    });

  };

  var initTree = function(){
      var _this     = this;

      this.treeView = $("#treeview").kendoTreeView({
        dataSource: this.getDS,
        dataTextField: "name",
        template: kendo.template($("#treeview-template").html()),
        dataBound: function(e){
          // Explicitly add rootfolder
          _this.insertRootFolder();
        }

      });
  };

  return {

    init: function(){
        modTreeDS.initTreeDS();
        modTreeDS.initTree();
    },

    getSelected: function(){
      var uid  = this.treeView.data("kendoTreeView").select().attr('data-uid');
      var item = this.getDS.getByUid(uid);
      return item;
    },

    insertRootFolder:function(){
      var rootID          = 0;
      var rootFolderExist = modTreeDS.getDS.get( rootID );

      if( rootFolderExist ) return;

        oFolder     =  {  id    : rootID,
                          ext   :'',
                          name  : $('legend').text() + ' (Module Level)',
                          type  :'folder',
                          st_id : docsJS.st_id
                        };

        modTreeDS.getDS.insert( 0, oFolder );
    }
};

})($, kendo);



var modMoveToFolder = (function($){

  var moveAjax = function( data ){

    $.post( modURL.move, data )

      .done( function(result){

        // Refresh datasource
        modDocDS.refresh();

        // Close kendoui window
        modWindowJS.close();

        // Notify user success
        toastr.success('Files were successfully moved');
      })

      .fail( function( event, jqxhr, errMsg, exception ){
        // Notify user error
        toastr.error('Files were not successfully moved');
        console.log(errMsg);
      });
  };

  return {
    init: function(){
      var arrChecked = modSelect.getArrChecked();
      var folderId   = modTreeDS.getSelected() ;

      if( !arrChecked.length ) {
        toastr.warning('No documents are selected in blue');
        return;
      }

      if( !folderId ) {
        toastr.warning('No folder was selected');
        return;
      }

      var data = {  folderID  : modTreeDS.getSelected().id,
                    docs      :  JSON.stringify( modSelect.getArrChecked() )
                  };

      this.moveAjax( data );

    }

  };

})($);



