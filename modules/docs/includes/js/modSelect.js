//////////////////
// modSelect //
//////////////////
var modSelect = {

  selectHandler: function(e){
    var item         = $(e.currentTarget);
    var type = item.find(':checkbox').attr('data-type');
    var name = item.find(':checkbox').attr('data-name');

    this.checkRowCheckbox(e);
    this.runRules();
  },

  runRules: function(  ){
    var arrChecked   = this.getArrChecked();
    var noneSelected = !arrChecked.length;
    var rowHeader    = $('.rowHeader');

    // Hide show header toolbar
    if( noneSelected ){
      rowHeader.show();
    }
    else{
      rowHeader.hide();
    }

  },

  unCheckAll : function(){
    var rowFiles = $('.docs .row.rowFiles');

    rowFiles.removeClass('selected');
    rowFiles.find(':checkbox').prop('checked',false);
    this.runRules();
  },

  checkAll : function(){
    var rowFiles = $('.docs .row.rowFiles');

    rowFiles.removeClass('selected');
    rowFiles.addClass('selected');
    rowFiles.find(':checkbox').prop('checked',true);
    this.runRules();
  },

  getArrChecked : function(){
   var checked    = this.getCheckedEl();
   var arrChecked = [];

   // Loop over checked checkbox elements
   checked.each( function(i, el){

     // Create array of doc objects
     arrChecked.push({
                       id   : $(el).val(),
                       type : $(el).attr('data-type'),
                       name : $(el).attr('data-name'),
                       ext  : $(el).attr('data-ext')
                     }
     );

   });

   return arrChecked;
  },

  checkRowCheckbox: function(e){
    var curTarget    =  $(e.currentTarget);
    var target       =  $(e.target);
    var thisCheckbox = curTarget.children().find( $(':checkbox') );
    var isChecked    = thisCheckbox.prop("checked");

    // On check toggle class .selected
    if( target.attr('type') === 'checkbox' ){
      if(isChecked){
        curTarget.addClass('selected');
      }
      else{
        curTarget.removeClass('selected');
      }

    }

    else{
      // On row select toggle class .selected and checkbox check property
      if(!isChecked){
        curTarget.addClass('selected');
      }
      else{
        curTarget.removeClass('selected');
      }

      thisCheckbox.prop('checked', !isChecked );
    }

  },

  getCheckedEl : function(){
    //Return all checked checkbox element
    return $('.rowFilesCheckbox').filter(':checked');
  }


};