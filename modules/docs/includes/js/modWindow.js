////////////
// modWindowJS //
////////////
var modWindowJS = {

  init: function( title ){
    $('.docs').append('<div id="docsWindow"></div>');
    this.modal = $("#docsWindow");
    this.addEventListeners();
    this.initModal( title );
    this.open();
  },

  addEventListeners: function () {
    $('body').on('click', '.k-window', function(e){ e.stopPropagation(); });

  },

  open: function(){
    this.modal.kendoWindow().data("kendoWindow").open();
    this.modal.kendoWindow().data("kendoWindow").center();
    $(".k-window").css({ top: 50 });
  },

  close: function(){
    $("#docsWindow").data('kendoWindow').destroy();
    $('.k-overlay').remove();

  },

  initModal: function( title ){
    var _this = this;
      this.modal.kendoWindow({
        width: "445px",
        modal: true,
        title: title,
        close: _this.close
      });
  },

};
