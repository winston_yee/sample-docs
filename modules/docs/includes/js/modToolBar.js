////////////////
// modToolbar //
////////////////
var modToolbar = {

  clickHandler: function(e){
    var target = $(e.currentTarget);
    var action = target.attr('data-action');

    switch( action ){
      case 'download':
        this.downloadFile();
        e.stopPropagation();
      break;

      case 'remove':
        this.removeDoc();
        e.stopPropagation();
      break;

      case 'rename':
        modRenameDoc.init();
        e.stopPropagation();
      break;

      case 'move':
        modMoveDoc.init();
        e.stopPropagation();
      break;

      case 'upload':
        modUpload.init();
        e.stopPropagation();
      break;

      case 'newFolder':
        modNewFolder.init();
        e.stopPropagation();
      break;

    }

  },

  downloadFile: function(){

    var arrChecked = modSelect.getArrChecked();
    var data       = { arrChecked: JSON.stringify( arrChecked ), pg_id: docsJS.pg_id };
    var isOk       = '';

    if( !arrChecked.length ){
      toastr.warning('Please select an item to download');
      return;
    }

    //Show loading icon
    kendoUtilJS.showLoading(true);

    $.post( modURL.download, data )

      .done( function(result){
        var url = docsJS.getCmdtsInfo.showFile + '?zip=' + result;

        // Serve file to user
        window.open(url , '_self','','');

        kendoUtilJS.showLoading(false);
     })

     .fail( function( event, jqxhr, errMsg, exception ){
        // Notify user error
        toastr.error('There was an error while downloading');
        console.log(errMsg);
     });

  },

  removeDoc: function(){

    var arrChecked = modSelect.getArrChecked();
    var data       = { arrChecked: JSON.stringify( arrChecked ) };
    var isOk       = '';
    var exit       = false;
    var canEdit    = false;
    var _this      = this;

    // Can this user edit this doc
    var canEditDocAjax  = $.post( modURL.canEditDoc, data );

    $.when( canEditDocAjax ).then( function( flag ){

      canEdit = utilJS.strToBool( flag );
      var msg = 'You do not have access to remove this document. Make sure you are the creator. Make sure there are no files in the folder';

      if( !canEdit ){
        toastr.warning( msg , '', {timeOut: 5000});
      }

    })

    .then( function(){

      if( canEdit ){
        // Does folder contain sub documents
        var containsDocAjax = $.post( modURL.containsDoc, data );
        var msg             = 'Do you want to delete the selected documents?';

        $.when( containsDocAjax ).then( function( flag ){

          msg = utilJS.strToBool( flag ) ? 'At least one folder contains files. ' + msg : msg;

          //Delete confirmation
          isOk = confirm( msg );

          if ( isOk ){
            _this.removeDocAjax( data );
          }

        });

      }
    });
  },

  removeDocAjax: function( data ){
    $.post( modURL.remove, data )

      .done( function(result){
        modDocDS.refresh();

        // Notify user success
        toastr.success('Files were removed');
      })

      .fail( function( event, jqxhr, errMsg, exception ){
        // Notify user error
        toastr.error('There was an error removing your files');
        console.log(errMsg);
      });
  }


};