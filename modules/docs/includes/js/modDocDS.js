////////////
// modDocDS //
////////////

var modDocDS = (function($, kendo){

  var parseDate = function ( rawDate ) {
    var newDate = kendo.parseDate(rawDate, "MM/dd/yy");
    return newDate;
  };

  return{
    // Setup DataSource
    init: function(){

      var _this = this;
      var data  = {  st_id    : docsJS.st_id,
                    pgm_id    : docsJS.pgm_id,
                    pgmDocID  : docsJS.pgmDocID,
                    pg_id     : docsJS.pg_id,
                    l_id      : docsJS.l_id,
                  };

      //Set getDS property on modDocDS
      this.getDS = new kendo.data.DataSource({
        transport: {
          read:  {
            url      : modURL.read,
            data     : data,
            dataType : "json"
          }
        },
        schema: {
            model: {

                fields: {
                  doe: {type: "date", parse: this.parseDate}
                }
            }
        },
        change: function(e) {
          // Render docs template
          _this.renderDocs();
         },
      });

      _this.refresh();

    },

    refresh: function(){
      this.getDS.read();
    },

    renderDocs: function(){
      // Get kendoui template in view
      var template        = kendo.template( $("#docsTemplate").html() );

      // Get model from kendoui datasource
      var data            = modDocDS.getDS.view();

      var combineTemplate = template(data);

      $('.docsTemplateContainer').html( combineTemplate );
    }

};

})($, kendo);

