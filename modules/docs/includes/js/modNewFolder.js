//////////////////
// modNewFolder //
//////////////////
var modNewFolder = {
  init: function(){
    this.newInput    = '';
    this.elDocName   = '';
    this.oFolder     =  { id    :'-1',
                          ext   :'',
                          name  :'',
                          type  :'folder',
                          st_id : docsJS.st_id
                        };

    this.createNewFolder();
  },

  createNewFolder: function(){

    //Insert in kendoui datasource and will render template row
    this.insertItemInDS( this.oFolder );

    // Insert input in new doc row
    this.insertInput();

    // Add event listerns on new input
    this.addEventListeners();
  },

  insertItemInDS: function( oFolder ){
    modDocDS.getDS.insert( 0, oFolder );
  },

  insertInput: function(){
    //Build input
    this.newInput = $('<input>').attr('type','text').attr('id','newFolder').val( '' );

    // Place input in row
    $('.rowFiles .docName') .first()
                            .append( this.newInput );

    // Put cursor in new input
    this.newInput.select();

  },

  addEventListeners: function(){
    var _this = this;

    this.newInput.on('keyup', function(e){

      //Press Enter
      if(e.keyCode === 13) {
          _this.insertFolderAjax();
        }

      //Press ESC
      else if(e.keyCode === 27){
        _this.removeFolder();
      }

    });

    // Revert back to original name if input loses focus
    this.newInput.on('focusout', function(e){
      _this.removeFolder();
    });
  },

  removeFolder: function(){
    var dataItem = modDocDS.getDS.at(0);

    // Remove in kendoui datasource
    modDocDS.getDS.remove( dataItem );

    // Remove new input
    this.newInput.remove();
  },

  insertFolderAjax: function(){
    var _this      = this;
    var folderName = this.newInput.val();
    var data       = this.oFolder;

    // Overwrite with new name
    data.name      = folderName;

    $.post( modURL.rename, data )

      .done( function(result){
        // Refresh kendoui datasoure and render template
        modDocDS.refresh();

        // Notify user success
        toastr.success( folderName + 'folder was created' , '', {timeOut: 7000} );
      })
      .fail( function( event, jqxhr, errMsg, exception ){
        // Notify user error
        toastr.error( folderName + 'folder could not be created' , '', {timeOut: 7000} );
        console.log(errMsg);
      });

  },

};